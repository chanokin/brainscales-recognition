import matplotlib.pyplot as plt
import numpy as np
import scipy
from scipy import misc as scipy_misc
from scipy import ndimage
import sys
import os
import zipfile
from mpl_toolkits.axes_grid1 import make_axes_locatable
import glob
from focal import *
from PIL import Image
import time
import cv2

def get_n_rows(cols, len_set):
    return (len_set // cols + (0 if (len_set % 2) == 0 else 1))


def extract_to_dict(input_path, file_depth=4, threshold=0.5):
    BASE, ALPHA, CHAR = range(3)

    zip_file = zipfile.ZipFile(input_path)
    d = {}
    for name in zip_file.namelist():
        split_path = name.split('/')

        if len(split_path) != file_depth:
            continue

        if split_path[-1] == '':
            continue

        if split_path[ALPHA] not in d:
            d[split_path[ALPHA]] = {}

        if split_path[CHAR] not in d[split_path[ALPHA]]:
            d[split_path[ALPHA]][split_path[CHAR]] = []

        f = zip_file.open(name)

        img =  np.asarray(Image.open(f)).astype('float')

        # invert image --- so high values mean 255 and low mean 0
        hi = np.where(img > threshold)
        lo = np.where(img <= threshold)
        img[lo] = 255.0
        img[hi] = 0.0

        d[split_path[ALPHA]][split_path[CHAR]].append(img)

    return d


def get_center_of_mass(image, threshold=1):
    rows, cols = np.where(image > threshold)
    return np.mean(rows), np.mean(cols)


def center_chars(char_dict, threshold=50):
    for alpha in char_dict:
        for char_id in char_dict[alpha]:
            for idx, img in enumerate(char_dict[alpha][char_id]):
                c0, r0 = img.shape
                c0 /= 2
                r0 /= 2
                ri, ci = get_center_of_mass(img)
                dr = r0 - ri
                dc = c0 - ci

                char_dict[alpha][char_id][idx][:] = \
                    ndimage.interpolation.shift(img, [dr, dc], mode='constant', cval=0)

                whr = np.where(char_dict[alpha][char_id][idx] > threshold)
                char_dict[alpha][char_id][idx][whr] = 255

                whr = np.where(char_dict[alpha][char_id][idx] <= threshold)
                char_dict[alpha][char_id][idx][whr] = 0


def normalize_pixels(char_dict, max_val=1000):
    for alpha in char_dict:
        for char_id in char_dict[alpha]:
            for idx, img in enumerate(char_dict[alpha][char_id]):
                minv = img.min()
                maxv = img.max()

                char_dict[alpha][char_id][idx][:] = \
                    (char_dict[alpha][char_id][idx] - minv) / (maxv - minv)


#                 char_dict[alpha][char_id][idx] /= \
#                     char_dict[alpha][char_id][idx].sum()

#                 char_dict[alpha][char_id][idx] *= max_val

def get_min_max_sizes(char_dict):
    max_r, max_c = -np.inf, -np.inf
    min_r, min_c = np.inf, np.inf

    for alpha in char_dict:
        for char_id in char_dict[alpha]:
            for idx, img in enumerate(char_dict[alpha][char_id]):
                rows, cols = np.where(img > 0)
                mxr = np.max(rows)
                mxc = np.max(cols)
                mnr = np.min(rows)
                mnc = np.min(cols)

                max_r = max(max_r, mxr)
                max_c = max(max_c, mxc)

                min_r = min(min_r, mnr)
                min_c = min(min_c, mnc)

    return (max_r, max_c), (min_r, min_c)


def get_minMax_maxMin_sizes(char_dict):
    maxMin_r, maxMin_c = np.inf, np.inf
    minMax_r, minMax_c = -np.inf, -np.inf

    for alpha in char_dict:
        for char_id in char_dict[alpha]:
            for idx, img in enumerate(char_dict[alpha][char_id]):
                rows, cols = np.where(img > 0)
                mxr = np.max(rows)
                mxc = np.max(cols)
                mnr = np.min(rows)
                mnc = np.min(cols)

                maxMin_r = min(maxMin_r, mxr)
                maxMin_c = min(maxMin_c, mxc)

                minMax_r = max(minMax_r, mnr)
                minMax_c = max(minMax_c, mnc)

    return (maxMin_r, maxMin_c), (minMax_r, minMax_c)


def clip_images(char_dict, max_size=64):
    (max_r, max_c), (min_r, min_c) = get_min_max_sizes(char_dict)

    diff_r = max_r - min_r
    diff_c = max_c - min_c

    img_size = max(max(diff_r, diff_c), max_size)
    img_size += (2 if max_size == img_size else 0)
    img_size += (1 if (img_size % 2) == 1 else 0)

    for alpha in char_dict:
        for char_id in char_dict[alpha]:
            for idx, img in enumerate(char_dict[alpha][char_id]):
                pad = (img.shape[0] - img_size) // 2
                end = img_size + pad
                char_dict[alpha][char_id][idx] = img[pad:end, pad:end]


def safe_dir_name(dir_in, replace_in=('(', ')', ' ',), replace_out=('-', '-', '_')):
    dir_out = dir_in
    for i, ch in enumerate(replace_in):
        dir_out = dir_out.replace(ch, replace_out[i])
    return dir_out

#########################################################################
#########################################################################
#########################################################################

print("Omniglot (shrunk) to spikes")

width = 56

out_shape = (width, width)
output_path = './omniglot/spikes_shrink_%d'%out_shape[0]
source_path = './omniglot/images_background.zip'
alphas = extract_to_dict(source_path)
num_chars = []
samples_per_char = []
total_total = 0.0
for a in alphas:
    num_chars.append(len(alphas[a]))
    for ch in alphas[a]:
        samples_per_char.append(len(alphas[a][ch]))
        total_total += float( len(alphas[a][ch]) )

n_chars = np.min(num_chars)


os.makedirs(output_path, exist_ok=True)
fcl = Focal(mute_output=True)
total_sum = 0.0
rank = []
ssa = []
for a in sorted(alphas.keys()):
    adir = os.path.join(output_path, safe_dir_name(a))
    os.makedirs(adir, exist_ok=True)
    per_alpha_total = len(alphas[a].keys()) * 20.0
    for ch_i, ch in enumerate(sorted(alphas[a].keys())):
        chdir = os.path.join(adir, ch)
        os.makedirs(chdir, exist_ok=True)
        for i, img in enumerate(alphas[a][ch]):
            small = np.clip(
                        cv2.resize(img,
                            dsize=out_shape,
                            interpolation=cv2.INTER_CUBIC),
                        0.0, 255.0)
            status = '\rShrunk --- {:6.2f}% of {}\t\t{:6.2f}% of total'.format(
                        100.0*(ch_i*20.0 + i + 1)/per_alpha_total,
                        a,
                        100.0*(total_sum + 1) / total_total)
            sys.stdout.write(status)
            sys.stdout.flush()
            fname = os.path.join(chdir, "sample_%02d.npz" % i)
            if not os.path.isfile(fname):
                rank[:] = fcl.apply(small, spikes_per_unit=0.1)

                ssa[:] = focal_to_spike(rank, small.shape, spikes_per_time_block=5,
                                        start_time=0., time_step=0.3)
                ### save to its file
                np.savez_compressed(fname, rank=rank, spike_source_array=ssa, image=small)
            else:
                time.sleep(0.01)

            total_sum += 1.0
            # break

        # break

    sys.stdout.write("\n")
    sys.stdout.flush()
    # break

# np.savez_compressed('rank_order_omniglot.npz', **spikes)