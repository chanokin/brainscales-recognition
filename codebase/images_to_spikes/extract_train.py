import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import os
import sys
from focal import *

from pyMNIST.read_mnist_img import *
from pyMNIST.read_mnist_label import *


mnist_dir = './mnist-db'
out_image_dir = os.path.join(mnist_dir, 'images', 'train')
out_spike_dir = os.path.join(mnist_dir, 'spikes', 'train')

if not os.path.isdir(out_image_dir):
  os.makedirs(out_image_dir)

if not os.path.isdir(out_spike_dir):
  os.makedirs(out_spike_dir)

for i in range(10):
  d = os.path.join(out_image_dir, str(i))
  if not os.path.isdir(d):
    os.makedirs(d)

  d = os.path.join(out_spike_dir, str(i))
  if not os.path.isdir(d):
    os.makedirs(d)

images_file = os.path.join(mnist_dir, 'train-images.idx3-ubyte')

labels_file = os.path.join(mnist_dir, 'train-labels.idx1-ubyte')

print("Loading labels")
mnist_lbls = read_label_file(labels_file)

print("Loading images")
mnist_imgs = read_img_file(images_file, labels=mnist_lbls)

print("Creating Focal object")
fcl = Focal(mute_output=True)
focal_spikes = []
spk_src_arr = []

keys = mnist_imgs.keys()
total_imgs =  float(len(keys))
count = 0.0
sys.stdout.write('\rMNIST to spikes is at %06.2f%%' % (100.0 * ((count + 1) / total_imgs)))
sys.stdout.flush()

for idx in keys:
  label = mnist_imgs[idx]["lbl"]
  _filename = "image__idx_%06d__lbl_%02d_"%(idx, label)

  img_filename = os.path.join(out_image_dir, str(label), _filename+'.png')
  sp.misc.imsave(img_filename, mnist_imgs[idx]["img"])

  focal_spikes[:] = fcl.apply(mnist_imgs[idx]["img"], spikes_per_unit=0.1)
  spk_src_arr[:] = focal_to_spike(focal_spikes, mnist_imgs[idx]["img"].shape,
                                  spikes_per_time_block=1, start_time=0., time_step=0.1)
  spk_filename = os.path.join(out_spike_dir, str(label), _filename+'.npz')
  np.savez_compressed(spk_filename, spikes=spk_src_arr, idx=idx, label=label)

  sys.stdout.write('\rMNIST to spikes is at %06.2f%%'%(100.0 * ((count+1) / total_imgs) ))
  sys.stdout.flush()

  count += 1.0


print("")
print('done!!!')